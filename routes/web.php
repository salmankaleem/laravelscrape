<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Search
Route::get('/Pages/search/Resultsempty', function(){
  return View::make('search.search-empty');
});

Route::get('/Pages/search/Results', 'SolariumController@search')->name('search');

// Solarium Routes (DEV TESTING ONLY)
Route::prefix('/solarium')->group(function() {
  Route::get('/ping', 'SolariumController@ping')->name('solr-ping');
  Route::get('/clean', 'SolariumController@clean')->name('solr-clean');
  Route::get('/reindex', 'SolariumController@reindex')->name('solr-reindex');
});
