@if ($paginator->hasPages())
    <div class="srch-Page srch-Page-bg">
        {{-- Previous Page Link --}}
        @if (!$paginator->onFirstPage())
            <span class="srch-Page-img">
                <a id="SRP_PrevImg" href="{{ $paginator->previousPageUrl() }}">
                    <img border="0" src="{{ asset('images/search/prev.png') }}" title="Move to previous page">
                </a>
            </span>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <span>{{ $element }}</span>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <strong>{{ $page }}</strong>
                    @else
                        <a href="{{ $url }}" title="Move to page {{ $page }}">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <span class="srch-Page-img">
                <a id="SRP_NextImg" href="{{ $paginator->nextPageUrl() }}">
                    <img border="0" src="{{ asset('images/search/next.png') }}" title="Move to next page">
                </a>
            </span>
        @endif
    </div>
@endif
