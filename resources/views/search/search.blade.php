@php
  $highlighting = $results->getHighlighting();
  $base_url     = url('/');
  $result_image = asset('images/search/html16.png');
  $solr_lang    = env('SOLR_LANG');
  $titleLang    = "title_{$solr_lang}";
  $contentLang  = "content_{$solr_lang}";
@endphp

<div class="srch-stats" accesskey="W">
  <b>{{ sprintf('%d-%d', $start + 1, $start + $results->count()) }}</b>
  of about {{ $results->getNumFound() }} results{{ $results->getQueryTime() ? ' Your search took ' . $results->getQueryTime() . ' seconds' : '' }}.
</div>

<div class="srch-results">
  @foreach($results as $document)
      @php
        $fields     = $document->getFields();
        $hlDocument = $highlighting->getResult($document->id);
        $hlTitle    = $hlDocument->getField("{$titleLang}");
        $hlContent  = $hlDocument->getField("{$contentLang}");
        $hlUrl      = $hlDocument->getField('url');
      @endphp
      <div class="srch-Icon">
        <img align="absmiddle" src="{{ $result_image }}" border="0" alt="Web Page">
      </div>
      <div class="srch-Title2">
        <div class="srch-Title3">
          <a style="font-size:14px; color: #0072bc !important;" href="{!! $fields['url'] !!}" title="{{ $fields["{$titleLang}"][0] }}">
            {!! $hlTitle[0] or $fields["{$titleLang}"][0] !!}
          </a>
        </div>
      </div>
      <div class="srch-Description2" style="max-width: 575px !important;">
        {!! $hlContent[0] or join( ' ', array_slice( explode( ' ', $fields["{$contentLang}"][0] ), 0, 20 ) ) !!}
      </div>
      <p class="srch-Metadata1" style="padding: 0 !important; margin: 2px 0 25px 24px; font-size: 15px; border: none !important;">
        <span><span class="srch-URL2" style="color: #008000 !important; max-width: 575px; word-wrap: break-word; margin-right: 8px;">{!! $hlUrl[0] or $fields['url'] !!}</span></span>
      </p>
  @endforeach
</div>

{{ $paginator->links('search.pagination') }}
