<?php

namespace App\Console\Commands;

use App\Console\Commands\SolrReindex\CrawlObserver;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\URL;
use App\Console\Commands\SolrReindex\CustomCrawler as Crawler;
use App\Console\Commands\SolrReindex\InternalUrlsProfile;
use Solarium\Client;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\RequestOptions;

class SolrReindex extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'solr:reindex
                            {--url= : the url to be crawler}
                            {--auth_user= : the user name for passing basic authentication}
                            {--auth_pass= : the user password for passing basic authentication}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run indexing process';

    /**
     * @var Client
     */
    protected $solariumClient;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct( Client $client ) {
        parent::__construct();

        $this->solariumClient = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $baseUrl = URL::to( '/' );

        if ( ! is_null( $this->option( 'url' ) ) ) {
            $baseUrl = $this->option( 'url' );
        }

        $cliet_args = [
            RequestOptions::ALLOW_REDIRECTS => false,
            RequestOptions::COOKIES         => true,
        ];

        if ( $this->hasOption( 'auth_user' ) && $this->hasOption( 'auth_pass' ) ) {
            $cliet_args[ RequestOptions::AUTH ] = [
                $this->option( 'auth_user' ),
                $this->option( 'auth_pass' ),
            ];
        }

        $client = new GuzzleClient( $cliet_args );

        $crawlProfile = new InternalUrlsProfile( $baseUrl );

        $crawler = new Crawler( $client );
        $crawler->setCrawlObserver( new CrawlObserver( $this->solariumClient ) )
                ->setCrawlProfile( $crawlProfile )
                ->startCrawling( $baseUrl );
    }
}
