<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Solarium\Client;

class SolrClean extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'solr:clean
                                {--domain= : setup domain filter}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleaning Solr collection';

    /**
     * @var Client
     */
    protected $solariumClient;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct( Client $client ) {
        parent::__construct();

        $this->solariumClient = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $query = '*:*';
        if ( $this->hasOption( 'domain' ) ) {
            $urlChunks = parse_url( $this->option( 'domain' ) );
            $host      = array_get( $urlChunks, 'host' ) ? array_get( $urlChunks, 'host' ) : array_get( $urlChunks, 'path' );
            $query     = sprintf( 'domain:%s', $host );
        }

        $update = $this->solariumClient->createUpdate();

        // add the delete query and a commit command to the update query
        $update->addDeleteQuery( $query );
        $update->addCommit();

        // this executes the query and returns the result
        $result = $this->solariumClient->update( $update );

        $this->info( 'Update query executed' );
        $this->info( 'Query status: ' . $result->getStatus() );
        $this->info( 'Query time: ' . $result->getQueryTime() );
    }
}
