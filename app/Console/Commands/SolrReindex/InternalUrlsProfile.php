<?php

namespace App\Console\Commands\SolrReindex;

use GuzzleHttp\Psr7\Uri;
use Spatie\Crawler\CrawlProfile;
use Spatie\Crawler\Url;

class InternalUrlsProfile implements CrawlProfile {
    protected $baseUrl;

    public function __construct( $baseUrl ) {
        if ( ! $baseUrl instanceof Url ) {
            $baseUrl = new Url( $baseUrl );
        }
        $this->baseUrl = $baseUrl;
    }

    public function shouldCrawl( Url $url ) {
        return $this->baseUrl->host === $url->host;
    }
}
