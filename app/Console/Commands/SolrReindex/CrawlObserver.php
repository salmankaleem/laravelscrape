<?php

namespace App\Console\Commands\SolrReindex;

use Spatie\Crawler\CrawlObserver as SpatieCrawlObserver;
use Spatie\Crawler\Url;
use Solarium\Client;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;

class CrawlObserver implements SpatieCrawlObserver {
  /**
   * @var Client
   */
  protected $solariumClient;

  protected $update;

  /**
   * List of documents be added into the Solr collection
   *
   * @var array
   */
  protected $documents = [];

  public function __construct( Client $client ) {
    $this->solariumClient = $client;

    $this->update = $this->solariumClient->createUpdate();
  }

  /**
   * Called when the crawler will crawl the url.
   *
   * @param \Spatie\Crawler\Url $url
   *
   * @return void
   */
  public function willCrawl( Url $url ) {
    echo $url . PHP_EOL;
  }

  /**
   * Called when the crawler has crawled the given url.
   *
   * @param \Spatie\Crawler\Url $url
   * @param \Psr\Http\Message\ResponseInterface|null $response
   * @param \Spatie\Crawler\Url $foundOnUrl
   *
   * @return void
   */
  public function hasBeenCrawled( Url $url, $response, Url $foundOnUrl = null ) {

    $solr_lang   = env('SOLR_LANG');

    echo $response->getStatusCode() . PHP_EOL;
    if ( $url && 200 == $response->getStatusCode() ) {

      $domCrawler = new DomCrawler( $response->getBody()->__toString(), (string) $url );

      $title     = $domCrawler->filter( 'title' );
      $content   = $domCrawler->filter( 'body' );

      $urlChunks = parse_url( $url );

      $titleLang   = "title_" . $solr_lang;
      $contentLang = "content_" . $solr_lang;

      // create a new document for the data
      $doc     = $this->update->createDocument();
      $doc->id = md5( $url );
      if ( $title->count() ) {
        $doc->{$titleLang} = $this->cleanText( $this->cleanHTML( $title->html() ) );
      }

      if ( $content->count() ) {
        $doc->{$contentLang} = $this->cleanText( $this->cleanHTML( $content->html() ) );
      }
      $doc->url    = $url;
      $doc->domain = preg_replace('/\s+/', '', array_get( $urlChunks, 'host' ) );

      if (!empty($doc->{$titleLang} && !empty($doc->{$contentLang}))) {
        $this->documents[] = $doc;
      }
    }
  }

  /**
   * Called when the crawl has ended.
   *
   * @return void
   */
  public function finishedCrawling() {
    $this->update->addDocuments( $this->documents );
    $this->update->addCommit();

    // this executes the query and returns the result
    $result = $this->solariumClient->update( $this->update );

    echo 'Update query executed' . PHP_EOL;
    echo 'Query status: ' . $result->getStatus() . PHP_EOL;
    echo 'Query time: ' . $result->getQueryTime() . PHP_EOL;
  }

  public function cleanText( $text ) {
    return preg_replace( '/\s+/s', ' ', strip_tags( $text ) );
  }

  /**
   * Strip html tags and also control characters that cause Jetty/Solr to fail.
   */
  function cleanHTML( $text ) {
    $text = preg_replace( '@<(applet|audio|canvas|command|embed|iframe|map|menu|noembed|noframes|noscript|script|style|svg|video)[^>]*>.*</\1>@siU', ' ', $text );

    $text = preg_replace( '/\s+/s', ' ', $text );

    return $text;
  }
}
