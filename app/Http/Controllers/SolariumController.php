<?php

namespace App\Http\Controllers;

use Solarium\Client;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Container\Container;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Artisan;

class SolariumController extends Controller {
  /**
   * @var Request
   */
  protected $request;

  /**
   * @var Client
   */
  protected $client;

  protected $num_rows_per_page = 10;

  public function __construct( Request $request, Client $client ) {
    $this->request = Request::capture();
    $this->client  = $client;
  }

  public function ping() {
    // create a ping query
    $ping = $this->client->createPing();

    // execute the ping query
    try {
      $this->client->ping( $ping );

      return response()->json( 'OK' );
    } catch ( \Solarium\Exception $e ) {
      return response()->json( 'ERROR', 500 );
    }
  }

  public function reindex() {
    $args = [];
    if ( $url = $this->request->get( 'url', url( '/' ) ) ) {
      $args['--url'] = $url;
    }

    if ( $this->request->has( 'auth_user' ) && $this->request->has( 'auth_pass' ) ) {
      $args['--auth_user'] = $this->request->get( 'auth_user', null );
      $args['--auth_pass'] = $this->request->get( 'auth_pass', null );
    }

    Artisan::call( 'solr:reindex', $args );

    return redirect( '/' );
  }

  public function clean() {
    $args = [];

    if ( $urlChunks = parse_url( $this->request->get( 'url', url( '/' ) ) ) ) {
      $args['--domain'] = array_get( $urlChunks, 'host' );
    }

    if ( $this->request->has( 'domain' ) ) {
      $args['--domain'] = $this->request->get( 'domain', null );
    }

    Artisan::call( 'solr:clean', $args );

    return redirect( '/' );
  }

  public function search() {
    $solr_lang   = env('SOLR_LANG');
    $titleLang   = "title_{$solr_lang}";
    $contentLang = "content_{$solr_lang}";
    $key         = $this->request->get( 'k', null );
    $currentPage = $this->request->get( 'start1', 1 );
    $urlChunks   = parse_url( $this->request->get( 'url', url( '/' ) ) );
    $perPage     = $this->num_rows_per_page;
    $start       = ( $currentPage - 1 ) * $perPage;

    $query = $this->client->createSelect();

    $filterQuery = $query->createFilterQuery()
                         ->setKey('domain')
                         ->addTag('domainLimit')
                         ->setQuery(sprintf( 'domain:%1$s', array_get( $urlChunks, 'host' ) ));

    $helper = $query->getHelper();
    $escaped_key = $helper->escapePhrase( $key );
    $query->setQuery("{$titleLang}:{$escaped_key} OR {$contentLang}:{$escaped_key} OR url:{$escaped_key}");
    $query->addFilterQuery( $filterQuery );
    $query->setStart( ( $currentPage - 1 ) * $perPage );
    $query->setRows( $perPage );

    // get highlighting component and apply settings
    $hl = $query->getHighlighting();
    $hl->setFields( "{$titleLang}, {$contentLang}, url" );
    $hl->setSimplePrefix( '<b>' );
    $hl->setSimplePostfix( '</b>' );

    $results = $this->client->select( $query );

    Paginator::resolveCurrentPage( 'start1' );

    $items     = collect( $results->getDocuments() );
    $total     = $results->getNumFound();
    $options   = [
      'path'     => Paginator::resolveCurrentPath(),
      'pageName' => 'start1',
      'query'    => [
        'k' => $key,
      ],
    ];
    $paginator = Container::getInstance()->makeWith( LengthAwarePaginator::class, compact(
      'items', 'total', 'perPage', 'currentPage', 'options'
    ) );

    if ( $results->count() ) {
      return view( 'search.search', compact( 'results', 'start', 'paginator' ) );
    }

    return view( 'search.search-empty' );
  }
}
